#!/bin/bash

IMAGE=mwc:0.0.1

docker image inspect $IMAGE  2>&1 > /dev/null
status=$?

if [ $status -eq 1 ]; then
    docker image build -t mwc:0.0.1 .
fi

docker run -it --rm --mount src=$PWD,target=/workdir,type=bind $IMAGE /bin/bash